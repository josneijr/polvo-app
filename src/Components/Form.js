import React, { Component } from 'react';
import '../Style/Form.css';
import mainLogo from '../logo_polvo_colorido.svg';
import InputField from './InputField.js'
import PasswordCheck from './PasswordCheck.js'

class Form extends Component {
  constructor(props){
    super(props);

    this.state = {
      passwordLength: false,
      passwordCapital: false,
      passwordNumber: false,
      conditionsOk: 0,
      firstPassword: "",
      secondPassword: ""
    }

    this.onPasswordChange = this.onPasswordChange.bind(this);
    this.onPasswordConfirm = this.onPasswordConfirm.bind(this);
    this.onSignUp = this.onSignUp.bind(this);
  }

  onPasswordChange(value) {
    var capitalRegex = /[A-Z]/g;
    var numbersRegex = /[\d]/g;

    var pswCapitals = capitalRegex.exec(value) !== null;
    var pswLength = value.length >= 6;
    var pswNumbers = numbersRegex.exec(value) !== null;
    var condOk = pswCapitals*1 + pswLength*1 + pswNumbers*1;

    this.setState({
      passwordLength: pswLength, 
      passwordCapital: pswCapitals,
      passwordNumber: pswNumbers,
      conditionsOk: condOk,
      firstPassword: value
    });
  }

  onPasswordConfirm(value) {
    this.setState({
      secondPassword: value
    });
  }

  onSignUp(){
    if(this.state.firstPassword !== this.state.secondPassword){
      alert("Senhas não coincidem!");
    }
    else{
      alert("Tudo certo!");
    }
  }

  render() {
    return (
      <div className="Form">
        <img src={mainLogo} alt="Logo here"/>

        <label className="label-purple">Seja bem-vindo</label>
        
        <InputField 
          labelText="E-mail"/>

        <InputField 
          labelText="Senha" 
          onchange={this.onPasswordChange}/>

        <PasswordCheck 
          pswLength={this.state.passwordLength}
          pswCapitals={this.state.passwordCapital}
          pswNumbers={this.state.passwordNumber} 
          conditionsOk={this.state.conditionsOk}/>
        
        <InputField 
          labelText="Confirme sua senha" 
          onchange={this.onPasswordConfirm}/>

        <button onClick={this.onSignUp}>
          Cadastrar
        </button>
      </div>
    );
  }
}

export default Form;