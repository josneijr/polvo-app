import React, { Component } from 'react';

class InputField extends Component {
  render() {
      return(
        <div className="input-field">
            <label>{this.props.labelText}</label>
            {this.props.onchange ? 
                <input onChange={e => this.props.onchange(e.target.value)}/>
                :
                <input/>
            }
        </div>
    )}
}

export default InputField;