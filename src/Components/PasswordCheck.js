import React, { Component } from 'react';

class PasswordCheck extends Component {
    constructor(props){
        super(props);

        this.rectanglesActive = this.rectanglesActive.bind(this);
    }

    rectanglesActive(numConditions, rectangleId){
        if(numConditions<rectangleId){
            return "";
        }
        
        if(numConditions === 1){
            return "rec-orange";
        }
        else if(numConditions === 1){
            return "rec-yellow";
        }
        else{
            return "rec-green";
        }
    }

    render() {
        return(
        <div className="password-check">
            <div className="rectangles">
                <div className={"rectangle " + this.rectanglesActive(this.props.conditionsOk, 1)}/>
                <div className={"rectangle " + this.rectanglesActive(this.props.conditionsOk, 2)}/>
                <div className={"last-rectangle " + this.rectanglesActive(this.props.conditionsOk, 3)}/>
            </div>
            <br/>
            <ul>
                <li className={this.props.pswLength===true ? "item-true": "item-false"}>
                    Pelo menos 6 caracteres
                </li>
                <li className={this.props.pswCapitals===true ? "item-true": "item-false"}>
                    Pelo menos 1 letra maiúscula
                </li>
                <li className={this.props.pswNumbers===true ? "item-true": "item-false"}>
                    Pelo menos 1 número
                </li>
            </ul>
        </div>
    )}
}

export default PasswordCheck;